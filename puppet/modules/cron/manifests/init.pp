class cron {
  
  package { 'cronie':
    ensure => installed
  }

  package { 'crontabs':
    ensure => installed
  }

}