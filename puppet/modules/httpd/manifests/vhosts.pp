class httpd::vhosts (
  $vhost_ip_address = $ipaddress,   # Default from Facter
  $vhost_port = 80,                 # Default: 80
  $vhost_basedir,                   # Required parameter
  $vhost_host = $fqdn,              # Default from Facter
  $vhost_host_alias = undef,        # Optional
  $vhost_proxy = undef,             # Optional
  $vhost_env = 'prod',              # Default: 'prod', possible values: 'prod', 'dev'
  )
{

  file { '/etc/httpd/sites-available': 
    ensure => directory,
  }

  file { '/etc/httpd/sites-enabled': 
    ensure => directory,
  }

  file { "/etc/httpd/sites-available/${vhost_host}.conf":
    ensure => file,
    content => template('httpd/vhosts.erb'),
    require => [ File['/etc/httpd/sites-available'], File['/etc/httpd/sites-enabled'] ]
  }

  file { "/etc/httpd/sites-enabled/${vhost_host}.conf":
    ensure => link,
    target => "/etc/httpd/sites-available/${vhost_host}.conf",
    require => File["/etc/httpd/sites-available/${vhost_host}.conf"],
  }

  # add sites-enabled to httpd.conf
  # @todo template
  exec { 'add sites-enabled':
    command => "printf 'NameVirtualHost *:80\nInclude /etc/httpd/sites-enabled/\n' >> /etc/httpd/conf/httpd.conf",
    unless => "grep -q 'NameVirtualHost *:80\nInclude /etc/httpd/sites-enabled/' '/etc/httpd/conf/httpd.conf'",
    notify => Service['httpd'],
    require => Package['httpd']
  }

  # local
  service {
    ['iptables', 'ip6tables']:
      ensure => stopped,
      enable => false
  }

  exec { 'httpd as vagrant':
    command => "sed -i 's/apache/vagrant/g' /etc/httpd/conf/httpd.conf",
    onlyif => "grep -q 'apache' '/etc/httpd/conf/httpd.conf'",
  #  notify => Service[httpd],
    require => Package['httpd']
  } ->

  file { '/var/lock/httpd':
    ensure => "directory",
    owner => "vagrant",
    group => "vagrant",
  }

  file { '/etc/hosts':
    ensure => file,
    content => file("httpd/hosts"),
  }

}