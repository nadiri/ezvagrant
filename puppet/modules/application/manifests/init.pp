class application {

  # timezone
  exec { 'set-timezone':
    path => ['/bin'],
    command => 'cp -f /usr/share/zoneinfo/Europe/Berlin /etc/localtime',
    logoutput => true
  }

  # useful packages
  package { 'screen':
    ensure => installed
  }

  package { 'wget':
    ensure => installed
  }

  package { 'ImageMagick':
    ensure => installed
  }

  package { 'varnish':
    ensure => installed,
    install_options => ['--enablerepo=epel'],
  }

  # disable SElinux only in local environment, could be checked with sestatus
  exec { 'selinux permissive':
    command => "setenforce 0",
    returns => [0, 1]
  }

  file { '/etc/selinux/config':
    ensure => file,
    content => "SELINUX=disabled"
  }

  exec { 'enable http in public zone':
    command => "firewall-cmd --permanent --zone=public --add-service=http; firewall-cmd --reload"
  }

  service { 'httpd':
    ensure => running,
    enable => true,
    require => [Package['php', 'httpd']]
  }

  # todo permissions

  # todo OpenOffice for odf support

  # todo JRE solr!
}