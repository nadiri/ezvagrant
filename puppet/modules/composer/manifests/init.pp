class composer {

  exec { 'composer.phar':
    command => "curl -sS https://getcomposer.org/installer | php",
    cwd     => "/usr/local/bin",
    creates => "/usr/local/bin/composer.phar",
    timeout => 0,
    require => [Package['php']]
  }

  exec { 'link composer':
    command => "/bin/ln -s /usr/local/bin/composer.phar /usr/local/bin/composer",
    path    => "/usr/local/bin:/usr/bin/:/bin",
    creates => "/usr/local/bin/composer",
    returns => [ 0, 1, '', ' ']
  }
}