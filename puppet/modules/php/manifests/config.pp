class php::config {

  file { '/etc/php.ini':
    owner   => root,
    group   => root,
    ensure  => file,
    mode    => 644,
    source  => "/etc/php.ini",
    require => Package['php'],
  }

  # todo template
  
  file_line{ 'memory_limit':
    path => "/etc/php.ini",
    line => "memory_limit = 512M",
  }

  file_line{ 'timezone':
    path => "/etc/php.ini",
    line => "date.timezone = \"Europe/Berlin\""
  }

# todo error log
# todo xdebug config

}