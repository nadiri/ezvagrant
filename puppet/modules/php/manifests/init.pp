class php {

  package { 'php':
    ensure => installed
  }

  package { 'php-cli':
    ensure =>  installed
  }

  # curl fileinfo json phar zip
  package { 'php-common':
    ensure =>  installed
  }

  package { 'php-pear':
    ensure =>  installed
  }

  package { 'php-xml':
    ensure =>  installed
  }

  package { 'php-gd':
    ensure =>  installed
  }

  package { 'php-intl':
    ensure =>  installed
  }

  package { 'php-mysql':
    ensure =>  installed
  }

  package { 'php-mbstring':
    ensure =>  installed
  }

# php-process? php-pecl-zendopcache?

}