class db {

  package { 'mariadb':
    ensure => installed
  }

  package { 'mariadb-server':
    ensure => installed
  }

}