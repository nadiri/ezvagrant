class db::config {

  exec { 'create database':
    command => "/usr/bin/mysql -uroot -e \"CREATE DATABASE IF NOT EXISTS ezpublish CHARACTER SET utf8;
    CREATE USER 'ez'@'localhost' IDENTIFIED BY 'password';
    GRANT ALL ON ezpublish.* TO 'ez'@'localhost';\"",
    unless => "/usr/bin/mysql -uroot ezpublish",
    require => Service['mariadb']
  }

  service { 'mariadb':
    ensure => running,
    enable => true,
    require => Package['mariadb-server']
  }

  # todo /etc/my.cnf

}