Exec { path => '/opt/ruby/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin' }

Package { allow_virtual => false }

include httpd
include httpd::vhosts
include php
#include php::config
include git
include composer
include cron
# include cron::config
include db
include db::config
include application
include ntp