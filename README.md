# eZVagrant #

## eZ Publish 5 in a vagrant box with CentOS 7 ##

### Setup ###

* VirtualBox Version 4.3.20
* Oracle_VM_VirtualBox_Extension_Pack-4.3.20-96996
* Vagrant 1.6.5


### Initialization ###

```bash
$ ./init-environment
$ vagrant up
```